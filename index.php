<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Document</title>
</head>
<style>
*{
    font-size:14px;
}
body{
    height:100vh;
    display:flex;
    flex-direction:column;
}
header{
    position:absolute;
    width:100%;
    z-index: 2;
}
.cards{
    border-radius:20px;
    position:relative;
    font-size:14px
}
.images::before{
    content:"";

}
/* .view-100{
    height:100vh;
} */
.contentHide{
    display:none;
}
.bg-grey{
    background-color: #e5e5e5;
}
.tables thead tr th{
    vertical-align: middle;
    border:2px solid #dee2e6;
    text-align:center;
}
.tables tbody tr td{
    vertical-align: middle;
    /* min-width:100px; */
    border:2px solid #dee2e6;
}
.tables tfoot tr td{
    vertical-align: middle;
    border:2px solid #dee2e6;
}
</style>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <a class="navbar-brand" href="#"><img src="images/bootstrap.png" width="150px" alt="" srcset=""></a>
            <div class="collapse navbar-collapse" id="navbarNav" style="min-width:150px">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><h3 class="m-0">Kementerian Sosial</h2></a>
                    </li>
                </ul>
            </div>
            <a class="navbar-brand" href="#"><img src="images/bootstrap.png" width="150px" alt="" srcset=""></a>
        </nav>
    </header>
    <section class="container-fluid mt-5 pt-5">
        <div class="row">
            <div id="div1" class="col-lg-12 contentHide" style="display:block">
                <table class="table tables">
                    <thead>
                        <tr>
                            <th class="bg-warning" rowspan="4">Kode</th>
                            <th class="bg-warning" rowspan="4">Kegiatan</th>
                            <th class="bg-warning" rowspan="4">PAGU</th>
                            <th class="bg-warning" class="text-center" colspan="9">Tahun Anggaran 2021</th>
                        </tr>
                        <tr>
                            <th class="bg-warning" colspan="9">TRIWULAN I</th>
                        </tr>
                        <tr>
                            <th class="bg-warning" colspan="3">JAN</th>
                            <th class="bg-warning" colspan="3">FEB</th>
                            <th class="bg-warning" colspan="3">MAR</th>
                        </tr>
                        <tr>
                            <th class="bg-warning">RENCANA</th>
                            <th class="bg-warning">REALISASI</th>
                            <th class="bg-warning">SISA</th>
                            <th class="bg-warning">RENCANA</th>
                            <th class="bg-warning">REALISASI</th>
                            <th class="bg-warning">SISA</th>
                            <th class="bg-warning">RENCANA</th>
                            <th class="bg-warning">REALISASI</th>
                            <th class="bg-warning">SISA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">A</td>
                            <td>Penyusun Materi</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                        </tr>
                        <tr>
                            <td class="text-center">A</td>
                            <td>Penyusun Materi</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td class="text-center"><b>Jumlah</b></td>
                            <td class="text-right">7,163,231</td>
                            <td colspan="2" class="text-center">7,163,231</td>
                            <td class="text-right">7,163,231</td>
                            <td colspan="2" class="text-center">7,163,231</td>
                            <td class="text-right">7,163,231</td>
                            <td colspan="2" class="text-center">7,163,231</td>
                            <td class="text-right">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-danger"><b>TOTAL PERSEN REALISASI S/D BULAN</b></td>
                            <td colspan="10" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-warning"><b>TOTAL RENCANA TRIWULAN</b></td>
                            <td colspan="10" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-warning"><b>TOTAL RENCANA TRIWULAN (%)</b></td>
                            <td colspan="10" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-info"><b>TOTAL REALISASI TRIWULAN</b></td>
                            <td colspan="10" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-info"><b>TOTAL REALISASI TRIWULAN (%)</b></td>
                            <td colspan="10" class="text-center">7,163,231</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div id="div2" class="col-lg-12 contentHide">
                <table class="table tables">
                    <thead>
                        <tr>
                            <th class="bg-warning" rowspan="4">Kode</th>
                            <th class="bg-warning" rowspan="4">Kegiatan</th>
                            <th class="bg-warning" rowspan="4">PAGU</th>
                            <th class="bg-warning" class="text-center" colspan="12">Tahun Anggaran 2021</th>
                        </tr>
                        <tr>
                            <th class="bg-warning" colspan="12">TRIWULAN I</th>
                        </tr>
                        <tr>
                            <th class="bg-warning" colspan="4">JAN</th>
                            <th class="bg-warning" colspan="4">FEB</th>
                            <th class="bg-warning" colspan="4">MAR</th>
                        </tr>
                        <tr>
                            <th class="bg-warning">BP</th>
                            <th class="bg-warning">BBO</th>
                            <th class="bg-warning">BBN</th>
                            <th class="bg-warning">BM</th>
                            <th class="bg-warning">BP</th>
                            <th class="bg-warning">BBO</th>
                            <th class="bg-warning">BBN</th>
                            <th class="bg-warning">BM</th>
                            <th class="bg-warning">BP</th>
                            <th class="bg-warning">BBO</th>
                            <th class="bg-warning">BBN</th>
                            <th class="bg-warning">BM</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">A</td>
                            <td>Penyusun Materi</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                        </tr>
                        <tr>
                            <td class="text-center">B</td>
                            <td>Koordinasi dan Pembahasan</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td class="text-center"><b>Jumlah</b></td>
                            <td class="text-right">7,163,231</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                            <td class="text-right">146.308</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center"><b>TOTAL KUMULATIF</b></td>
                            <td class="text-right">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-warning"><b>TOTAL KUMULATIF BELANJA PEGAWAI</b></td>
                            <td class="text-right bg-warning">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-info"><b>TOTAL KUMULATIF BELANJA BARANG OPERASIONAL</b></td>
                            <td class="text-right bg-info">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-success"><b>TOTAL KUMULATIF BELANJA BARANG NON OPERASIONAL</b></td>
                            <td class="text-right bg-success">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center bg-danger"><b>TOTAL KUMULATIF BELANJA MODAL</b></td>
                            <td class="text-right bg-danger">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                            <td colspan="4" class="text-center">7,163,231</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div id="div3" class="col-lg-12 contentHide">
                <table class="table tables">
                    <thead>
                        <tr>
                            <th class="bg-warning" rowspan="3">Kode</th>
                            <th class="bg-warning" rowspan="3">Kegiatan</th>
                            <th class="bg-warning" colspan="12">TRIWULAN I</th>
                        </tr>
                        <tr>
                            <th class="bg-warning" colspan="4">JAN</th>
                            <th class="bg-warning" colspan="4">FEB</th>
                            <th class="bg-warning" colspan="4">MAR</th>
                        </tr>
                        <tr>
                            <th class="bg-warning">1</th>
                            <th class="bg-warning">2</th>
                            <th class="bg-warning">3</th>
                            <th class="bg-warning">4</th>
                            <th class="bg-warning">1</th>
                            <th class="bg-warning">2</th>
                            <th class="bg-warning">3</th>
                            <th class="bg-warning">4</th>
                            <th class="bg-warning">1</th>
                            <th class="bg-warning">2</th>
                            <th class="bg-warning">3</th>
                            <th class="bg-warning">4</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center bg-success"><b>5254.AEA</b></td>
                            <td class="bg-success"><b>Koordinasi</b></td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                            <td class="text-right bg-success">52.800</td>
                        </tr>
                        <tr>
                            <td class="text-center"><b>AEA.001</b></td>
                            <td><b>Koordinasi Pengawasan Orang Asing</b></td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                        </tr>
                        <tr>
                            <td class="text-center">051</td>
                            <td>Koordinasi Pengawasan Orang Asing</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                            <td class="text-right">52.800</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <footer class="bg-light mt-auto border-top pt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 mx-auto text-center">
                    <div class="row">
                        <div class="col-lg mb-3"><img width="150px" src="images/bootstrap.png" alt="" srcset=""></div>
                        <div class="col-lg mb-3"><img width="150px" src="images/bootstrap.png" alt="" srcset=""></div>
                        <div class="col-lg mb-3"><img width="150px" src="images/bootstrap.png" alt="" srcset=""></div>
                        <div class="col-lg mb-3"><img width="150px" src="images/bootstrap.png" alt="" srcset=""></div>
                        <div class="col-lg mb-3"><img width="150px" src="images/bootstrap.png" alt="" srcset=""></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        setTimeout(function(){
            page2()
        }, 5000);
    })


    function Page1(){
        $('.contentHide').hide()
        $('#div1').fadeIn('8000')
        setTimeout(function(){
            page2()
        }, 5000);
    }

    function page2(){
        $('.contentHide').hide()
        $('#div2').fadeIn('8000')
        setTimeout(function(){
            back3()
        }, 5000);
    }

    function back3(){
        $('.contentHide').hide()
        $('#div3').fadeIn('8000')
        setTimeout(function(){
            Page1()
        }, 5000);
    }
</script>
</html>